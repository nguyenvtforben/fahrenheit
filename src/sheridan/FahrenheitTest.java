/**
 * author Tuan Nguyen Viet
 */
package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class FahrenheitTest {

	@Test
	public void testFromCelciusNormal() {
		double result = FarenhietConverter.fromCelcius("15");
		assertTrue("Incorrect Conversion", result == 59);
	}
	
	@Test(expected=NumberFormatException.class)
	public void testFromCelciusExceptional() {
		double result = FarenhietConverter.fromCelcius("aasd");
		assertFalse("Incorrect Conversion", result == 32);
	}
	
	@Test
	public void testFromCelciusBoundaryIn() {
		double result = FarenhietConverter.fromCelcius("0");
		assertTrue("Incorrect Conversion", result == 32);
	}
	
	@Test(expected=NumberFormatException.class)
	public void testFromCelciusBoundaryOut() {
		double result = FarenhietConverter.fromCelcius("0a");
		assertFalse("Incorrect Conversion", result == 32);
	}

}
