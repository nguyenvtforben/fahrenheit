/**
 * author Tuan Nguyen Viet
 */
package sheridan;

public class FarenhietConverter {

	public static int fromCelcius(String input) throws NumberFormatException{
		double temp = Double.parseDouble(input);
		return (int)Math.round((temp*9/5)+32);
	}
	
}
